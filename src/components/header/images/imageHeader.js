import image from '../../../assets/images/software-developer-6521720_640.webp';
import { Col, Row } from "reactstrap";

function ImageHeader() {

    return (
        <Row>
            <Col sm={12} md={12} lg={12}>
                <img src={image} alt='Chào mừng đến với Devcamp120' width={'600px'}></img>
            </Col>
        </Row>

    )

}
export default ImageHeader

