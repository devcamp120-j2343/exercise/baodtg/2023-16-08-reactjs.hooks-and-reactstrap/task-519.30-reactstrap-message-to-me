import like_image from '../../../assets/images/like_PNG87.webp'

function OutputContent({outputMessageProps, likeDisplayProps}) {


    return (
        <>
            <div className='row'>
                <div className='col-12 mt-2'>
                    {outputMessageProps.map((element, index) => {
                        return <p key={index}>{element}</p>
                    })}

                </div>
            </div>
            <div className='row'>
                <div className='col-12 mt-2'>
                    {likeDisplayProps ? <img src={like_image} alt="like icon" width={"70px"} /> : <></>}

                </div>
            </div>
        </>
    )

}
export default OutputContent