import { useState } from "react";
import InputContent from "./input/InputContent";
import OutputContent from "./output/OutputContent";

function Content () {
    const [inputMessage, setInputMessage] = useState("");
    const [outputMessage, setOutputMessage] = useState([]);
    const [likeDisplay, setLikeDisplay] = useState(false);
  
   const inputMessageChangeHandler = (value) => {
    setInputMessage(value)
       
    }
   const outputMessageChaneHandler = () => {
        if (inputMessage) {
            setOutputMessage([...outputMessage, inputMessage]);
            setLikeDisplay(true)
          
        }
        else {
            setOutputMessage([...outputMessage]);
            setLikeDisplay(false);

          
           
        }

    }
   
        return (
            <>
                <InputContent inputMessageProps={inputMessage} inputMessageChangeHandlerProps={inputMessageChangeHandler} outputMessageChangeHandlerProps={outputMessageChaneHandler} />
                <OutputContent outputMessageProps={outputMessage} likeDisplayProps={likeDisplay} />
            </>
        )
    
}
export default Content