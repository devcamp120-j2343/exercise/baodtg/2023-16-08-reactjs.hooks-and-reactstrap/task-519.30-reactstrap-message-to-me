import { Button, Col, Input, Label, Row } from "reactstrap";

function InputContent({inputMessageProps, inputMessageChangeHandlerProps, outputMessageChangeHandlerProps}) {
   const onInputChangeHandler = (event) => {
        let value = event.target.value
        console.log(event.target.value)
        inputMessageChangeHandlerProps(value)
    }
    const onButtonClickHandler = () => {
        console.log("Ấn nút gửi thông điệp!")
       outputMessageChangeHandlerProps();
    }

    return (
        <>
            <Row>
                <Col className="mt-2">
                    <Label>Message cho bạn 12 tháng tới:</Label>
                    <Input className='form-control w-75 mx-auto' placeholder='Nhập lời nhắn cho bạn...' onChange={onInputChangeHandler} value={inputMessageProps} />
                </Col>

            </Row>
            <Row>
                <Col className="mt-2">
                    <Button color="success" onClick={onButtonClickHandler}>Gửi thông điệp</Button>

                </Col>
            </Row>
            {/* <div className='row'>
                    <div className='col-12 mt-2'>
                        <label>Message cho bạn 12 tháng tới:</label>
                    </div>
                </div>
                <div className='row'>
                    <div className='col-12 mt-2'>
                        <input className='form-control w-75 mx-auto' placeholder='Nhập lời nhắn cho bạn...' onChange={this.onInputChangeHandler} value={this.props.inputMessageProps} />
                    </div>
                </div>
                <div className='row'>
                    <div className='col-12 mt-2'>
                        <button className='btn btn-success' onClick={this.onButtonClickHandler}>Gửi thông điệp</button>
                    </div>
                </div> */}
        </>
    )

}
export default InputContent